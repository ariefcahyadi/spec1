import React, { Component } from 'react'
import { Table } from 'reactstrap';
import { CSVReader } from 'react-papaparse'
import Container from 'react-bootstrap/Container'
import Hay from '../components/gila'
import { CSVLink, CSVDownload } from "react-csv";
import { Form, Button } from 'react-bootstrap'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';

const buttonRef = React.createRef()
const vcRef = React.createRef()

export default class Halo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      proses: '',
      hasil: [],
      ebom: null,
      speakerabnormallist: [],
      speakerabnormal: [],
      speakernormal: [],
      speakernormallist: [],
      hasiltotal: [],
      voicecoil: null,
      hasilvc: [],
      proseslanjut:''
    }
  }

  handleOpenDialog = (e) => {
    if (buttonRef.current) {
      buttonRef.current.open(e)
    }
  }
  handleOpenDialogvc = (e) => {
    if (vcRef.current) {
      vcRef.current.open(e)
    }
  }

  handleOnFileLoadvc = (data) => {
    if (data[0].data[0] == "Item Number" && data[0].data[3] == "Toleransi ID max") {
      data.splice(0, 1)
      let hasil = []
      data.map((val) => {
        if (val.data) {
          let od = (parseFloat(val.data[7]) > 0 ? parseFloat(val.data[7]) : 0).toFixed(2)
          let odtolmax = (parseFloat(val.data[8]) > 0 ? parseFloat(val.data[8]) : 0).toFixed(2)
          let odmax = (parseFloat(od) + parseFloat(odtolmax)).toFixed(2)
          hasil.push({
            vc: val.data[0],
            vcod: parseFloat(od).toFixed(2),
            vcodtolmax: parseFloat(odtolmax).toFixed(2),
            vcodmax: parseFloat(odmax).toFixed(2)
          })
        }
      })
      console.log(hasil)
      this.setState({ hasilvc: hasil, voicecoil: true })
    } else {
      this.setState({ voicecoil: false })
    }
  }

  handleOnFileLoad = (data) => {
    if (data[3].data[1] == "Level" && data[3].data[2] == "Parent Item") {
      data.splice(0, 4)
      let hasil = []
      data.map((val) => {
        hasil.push({
          'id': val.data[0],
          'level': val.data[1],
          'speaker': val.data[2],
          desc: val.data[3],
          'part': val.data[4],
          'part_desc': val.data[5]
        })
      })
      this.setState({ hasil: hasil, ebom: true, jumlahebom: hasil.length })

    } else {
      this.setState({ ebom: false })
    }
  }

  handleOnError = (err, file, inputElem, reason) => {
    console.log(err)
  }
  handleOnErrorvc = (err, file, inputElem, reason) => {
    console.log(err)

  }

  handleOnRemoveFile = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.setState({ ebom: null })
  }

  handleRemoveFile = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (buttonRef.current) {
      buttonRef.current.removeFile(e)
    }
    this.setState({ ebom: false })
  }
  handleRemoveFilevc = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (vcRef.current) {
      vcRef.current.removeFile(e)
    }
    this.setState({ voicecoil: null })
  }

  handleOnRemoveFilevc = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.setState({ voicecoil: null })
  }

  calculateebom = () => {
    this.setState({ proses: 'otw' })
    setTimeout(() => this.calculateebomnext(), 10)


  }
  calculateebomnext = () => {
    let { hasil, hasilvc } = this.state
    let hasiltotal = hasil.filter(val => {
      return val.level == "1"
    })
    hasil = hasil.filter(val => {
      if (val.part) {
        return val.part.slice(0, 3) == "2VC" || val.part.slice(0, 3) == "3TP"
      }
    })
    let speaker = [...new Set(hasil.map(item => item.speaker))];
    let speakernormal = []
    let speakerabnormal = []
    let speakernormallist = []
    let speakerabnormallist = []
    speaker.map(val => {
      let jumlahlv1 = hasiltotal.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.level == "1" && v.part_desc.includes("WF")
        }
      }).length

      console.log(jumlahlv1)
      let voicecoil = hasil.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.part.slice(0, 3) == "2VC"
        }
      })
      let levelvoicecoil = 10
      voicecoil.map(v => {
        if (parseInt(v.level) < levelvoicecoil) {
          levelvoicecoil = parseInt(v.level)
        }
      })
      let jumlahvc = voicecoil.filter(v => {
        return parseInt(v.level) == levelvoicecoil
      })

      let topplate = hasil.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.part.slice(0, 3) == "3TP"
        }
      })

      let leveltopplate = 10
      topplate.map(v => {
        if (parseInt(v.level) < leveltopplate) {
          leveltopplate = parseInt(v.level)
        }
      })
      let jumlahtopplate = topplate.filter(v => {
        return parseInt(v.level) == leveltopplate
      })

      if (jumlahvc.length == 1 && jumlahtopplate.length == 1 && jumlahlv1 == 0) {
        speakernormallist.push(val)
        let vc = hasilvc.find(y => { return y.vc == jumlahvc[0].part })
        speakernormal.push({
          speaker: val,
          desc: jumlahvc[0].desc,
          vc: jumlahvc[0].part,
          tp: jumlahtopplate[0].part,
          vcod: vc ? vc.vcod : 0,
          vcodtolmax: vc ? vc.vcodtolmax : 0,
          vcodmax: vc ? vc.vcodmax : 0,
        })
      } else if (jumlahvc.length > 0 && jumlahtopplate.length > 0) {
        speakerabnormallist.push(val)
      }

    })
    console.log(speakernormal)
    console.log(speakernormallist)
    this.setState({
      speakerabnormallist: speakerabnormallist,
      speakerabnormal: speakerabnormal,
      speakernormal: speakernormal,
      speakernormallist: speakernormallist,
      hasiltotal: hasiltotal,
      proses: 'done'
    })


  }
  calculatelanjut = () => {
    this.setState({ proseslanjut: 'otw' })
    setTimeout(() => this.calculatelanjutnext(), 10)
  }

  calculatelanjutnext = () => {
    let { speakerabnormallist, hasiltotal, speakernormal, speakerabnormal, speakernormallist, proseslanjut } = this.state
    speakerabnormallist.map(val => {
      console.log('jalananeh')
      let abnormallv1 = hasiltotal.filter(v => {
        return v.level == "1" && speakernormallist.includes(v.part) && v.speaker == val
      })
      let partabnormal = []
      abnormallv1.map(v => {
        partabnormal.push(speakernormal.find(w => {
          return w.speaker == v.part
        }))
      })
      if (partabnormal.length > 0) {
        speakerabnormal.push({
          speaker: val, desc: abnormallv1[0].desc, subspeaker: partabnormal
        })
      }
    })
    console.log(speakerabnormal)
    this.setState({ speakerabnormal: speakerabnormal, proseslanjut: 'done' })
  }


  render() {
    let { ebom, jumlahebom, hasilcone, cone, voicecoil, hasilvc, proses, speakernormal, proseslanjut, speakerabnormal } = this.state
    let csvDataNormal = []
    let csvDataSpecial = []
    return (
      <>
        <Hay title='Part To Part Analysis (Voice Coil and Top Plate)'>
          <Container>
            <div className="m-lg-3">
              <CSVReader
                ref={buttonRef}
                onFileLoad={this.handleOnFileLoad}
                onError={this.handleOnError}
                onDrag={this.handleOnFileLoad}
                addRemoveButton
                onRemoveFile={this.handleOnRemoveFile}
              >
                <span>Click to upload E-BOM</span>
              </CSVReader>
              <div className="mt-3">
                {
                  ebom == null ? (
                    <Alert variant={'warning'}>
                      Anda belum masukin Ebom
                    </Alert>
                  ) : (ebom == true ? (
                    <Alert variant={'primary'}>
                      {jumlahebom ? jumlahebom : 0} Data Ebom Berhasil di Upload
                    </Alert>
                  ) : (
                      <Alert variant={'danger'}>
                        Ebom anda salah format, tolong perbaiki
                      </Alert>
                    ))
                }
              </div>
            </div>
            {ebom ? (
              <div className="m-lg-3">
                <CSVReader
                  ref={vcRef}
                  onFileLoad={this.handleOnFileLoadvc}
                  onError={this.handleOnError}
                  onDrag={this.handleOnFileLoadvc}
                  addRemoveButton
                  onRemoveFile={this.handleOnRemoveFilevc}
                >
                  <span>Click to upload Database Voice Coil</span>
                </CSVReader>
                <div className="mt-3">
                  {
                    voicecoil == null ? (
                      <Alert variant={'warning'}>
                        Anda belum masukin Database Voice Coil
                      </Alert>
                    ) : (voicecoil == true ? (
                      <Alert variant={'primary'}>
                        {hasilvc.length > 0 ? hasilvc.length : 0} Data Voice Coil Berhasil di Upload
                      </Alert>
                    ) : (
                        <Alert variant={'danger'}>
                          Database Voice Coil anda salah format, tolong perbaiki
                        </Alert>
                      ))
                  }

                </div>
              </div>) : false}

            <div className="m-3">
              {proses == 'otw' ? (
                <Alert variant={'warning'}>Masih Proses, Harap Tunggu</Alert>
              ) : proses == 'done' ? (<Alert variant={'success'}>Data Sudah Siap</Alert>) : false}
            </div>
            {voicecoil == true ? (
              <Button variant="primary" className="m-3"
                onClick={this.calculateebom
                }
                size="lg" block>
                Quick Count
              </Button>) : false
            }
          </Container>
          {
            speakernormal.length > 0 ? (
              <Container>
                <Table responsive striped bordered hover size="sm" style={{ fontSize: '0.6rem' }}>
                  <thead>
                    <tr>
                      {
                        ['No',
                          'Speaker',
                          'Deskripsi',
                          '3TP',
                          '2VC',
                          'OD Gulungan',
                          'OD Gulungan Tol Max',
                          'OD Gulungan Max'
                        ].map(val => {
                          return <th>{val}</th>
                        })

                      }
                      {
                        csvDataNormal.push(['No',
                          'Speaker',
                          'Deskripsi',
                          '3TP',
                          '2VC',
                          'OD Gulungan',
                          'OD Gulungan Tol Max',
                          'OD Gulungan Max'])
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {speakernormal.map((val, i) => {
                      csvDataNormal.push([i + 1,
                      val.speaker,
                      val.desc,
                      val.tp,
                      val.vc,
                      val.vcod,
                      val.vcodtolmax,
                      val.vcodmax,
                      ])

                      return (
                        <tr>
                          {
                            [i + 1,
                            val.speaker,
                            val.desc,
                            val.tp,
                            val.vc,
                            val.vcod,
                            val.vcodtolmax,
                            val.vcodmax,].map(v => {
                              return <td>{v}</td>
                            })
                          }
                        </tr>)
                    })}
                  </tbody>
                </Table>
                <CSVLink data={csvDataNormal}>
                  <Button variant="success">Export ke CSV</Button>
                </CSVLink>

                <div className="m-3">
                  {proseslanjut == 'otw' ? (
                    <Alert variant={'warning'}>Jalan Masih Panjang, Harap Bersabar Guys</Alert>
                  ) : proseslanjut == 'done' ? (<Alert variant={'success'}>Data Sudah Siap</Alert>) : false}
                </div>
                <Button variant="danger" className="m-3"
                  onClick={this.calculatelanjut
                  }
                  size="lg" block>
                  Detailed Count
              </Button>



              </Container>) : false}
          {speakerabnormal.length > 0 ? (
            <Container>
              <Table responsive striped bordered hover size="sm" style={{ fontSize: '0.6rem' }}>
                <thead>
                  <tr>
                    {
                      ['No',
                        'Speaker',
                        'Deskripsi',
                        'Sub Speaker',
                        'Deskripsi',
                        '3TP',
                        '2VC',
                        'OD Gulungan',
                        'OD Gulungan Tol Max',
                        'OD Gulungan Max'
                      ].map(val => {
                        return <th>{val}</th>
                      })
                    }
                    {
                      csvDataSpecial.push(['No',
                        'Speaker',
                        'Deskripsi',
                        'Sub Speaker',
                        'Deskripsi',
                        '3TP',
                        '2VC',
                        'OD Gulungan',
                        'OD Gulungan Tol Max',
                        'OD Gulungan Max'
                      ])
                    }
                  </tr>
                </thead>

                <tbody>
                  {speakerabnormal.map((y, i) => {
                    let no = i + 1
                    return (
                      y.subspeaker.map((val) => {
                        csvDataSpecial.push([no,
                          y.speaker,
                          y.desc,
                          val.speaker,
                          val.desc,
                          val.tp,
                          val.vc,
                          val.vcod,
                          val.vcodtolmax,
                          val.vcodmax,])
                        return (
                          <tr>{
                            [no,
                              y.speaker,
                              y.desc,
                              val.speaker,
                              val.desc,
                              val.tp,
                              val.vc,
                              val.vcod,
                              val.vcodtolmax,
                              val.vcodmax,].map(v => {
                                return <td>{v}</td>
                              })
                          }
                          </tr>
                        )
                      })

                    )

                  })}
                </tbody>
              </Table>
              <CSVLink data={csvDataSpecial}>
                <Button variant="success">Export ke CSV</Button>
              </CSVLink>
            </Container>
          ) : false}
        </Hay>
      </>

    )
  }
}
