import React, { Component } from 'react'
import { Table } from 'reactstrap';
import { CSVReader } from 'react-papaparse'
import Container from 'react-bootstrap/Container'
import Hay from '../components/gila'
import { CSVLink, CSVDownload } from "react-csv";
import { Form, Button } from 'react-bootstrap'
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';

const buttonRef = React.createRef()
const coRef = React.createRef()
const vcRef = React.createRef()


export default class Halo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      proses: '',
      hasil: [],
      ebom: null,
      speakerabnormallist: [],
      speakerabnormal: [],
      speakernormal: [],
      speakernormallist: [],
      hasiltotal: [],
      speakernormal: [],
      hasilcone: [],
      cone: null,
      voicecoil: null,
      hasilvc: [],
      hide: false,
      proseslanjut: ''
    }
  }

  handleOpenDialog = (e) => {
    if (buttonRef.current) {
      buttonRef.current.open(e)
    }
  }
  handleOpenDialogco = (e) => {
    if (coRef.current) {
      coRef.current.open(e)
    }
  }
  handleOpenDialogvc = (e) => {
    if (vcRef.current) {
      vcRef.current.open(e)
    }
  }

  handleOnFileLoadco = (data) => {
    if (data[0].data[0] == "Item Number" && data[0].data[1] == "Desc 1") {
      data.splice(0, 1)
      let hasil = []

      data.map((val) => {
        let tolmin = val.data[14] == "GEN" ? 0.1 : parseFloat(val.data[14])
        let tolmax = val.data[13] == "GEN" ? 0.1 : parseFloat(val.data[13])
        let id = parseFloat(val.data[11])
        if (id && tolmin && tolmax) {
          hasil.push({
            'cone': val.data[0],
            'id': id,
            'tolmin': tolmin,
            'tolmax': tolmax,
            idmin: parseFloat((id - tolmin).toFixed(2)),
            idmax: parseFloat((id + tolmax).toFixed(2))
          })
        }
      })
      console.log(hasil)
      this.setState({ hasilcone: hasil, cone: true })
    } else {
      this.setState({ cone: false })
    }


  }
  handleOnFileLoadvc = (data) => {
    if (data[0].data[0] == "Item Number" && data[0].data[3] == "Toleransi ID max") {
      data.splice(0, 1)
      let hasil = []
      data.map((val) => {
        if (val.data) {
          console.log(data[35])
          let id = (parseFloat(val.data[2]) > 0 ? parseFloat(val.data[2]) : 0).toFixed(2)
          let idmax = (parseFloat(val.data[5]) > 0 ? parseFloat(val.data[5]) : 0).toFixed(2)
          let tebalformer = (parseFloat(val.data[17]) > 0 ? parseFloat(val.data[17]) : 0).toFixed(2)
          let tolmaxtebalformer = (parseFloat(val.data[18]) > 0 ? parseFloat(val.data[18]) : 0).toFixed(2)
          let tebaltaping1 = (parseFloat(val.data[23]) > 0 ? parseFloat(val.data[23]) : 0).toFixed(2)
          let tebaltaping2 = (parseFloat(val.data[26]) > 0 ? parseFloat(val.data[26]) : 0).toFixed(2)
          let diawire = (parseFloat(val.data[29]) > 0 ? parseFloat(val.data[29]) : 0).toFixed(2)
          let direct = val.data[35] ? (val.data[35].includes('non') ? false : true) : false
          let diawireap = direct ? 0 : diawire
          let isTwoSide = parseFloat(val.data[29]).includes('two')?true:false
          let isDC = parseFloat(val.data[42]).includes('DC')?true:false
          diawireap=diawireap*(isTwoSide||isDC?2:1)
          let odaplikasi = (parseFloat(idmax) + (parseFloat(tebalformer) * 2) + (parseFloat(tolmaxtebalformer) * 2) + (parseFloat(tebaltaping1) * 2) + (parseFloat(tebaltaping2) * 2) + (parseFloat(diawireap)*1)).toFixed(2)
          hasil.push({
            partnum: val.data[0],
            id: id,
            idmax: idmax,
            tebalformer: tebalformer,
            tolmaxtebalformer: tolmaxtebalformer,
            tebaltaping1: tebaltaping1,
            tebaltaping2: tebaltaping2,
            diawire: diawire,
            odaplikasi: odaplikasi
          })
        }
      })
      console.log(hasil)
      this.setState({ hasilvc: hasil, voicecoil: true })
    } else {
      this.setState({ voicecoil: false })
    }


  }

  handleOnFileLoad = (data) => {
    if (data[3].data[1] == "Level" && data[3].data[2] == "Parent Item") {
      data.splice(0, 4)
      let hasil = []
      data.map((val) => {
        hasil.push({
          'id': val.data[0],
          'level': val.data[1],
          'speaker': val.data[2],
          desc: val.data[3],
          'part': val.data[4],
          'part_desc': val.data[5]
        })
      })
      this.setState({ hasil: hasil, ebom: true, jumlahebom: hasil.length })

    } else {
      this.setState({ ebom: false })
    }



  }

  handleOnError = (err, file, inputElem, reason) => {
    console.log(err)
  }
  handleOnErrorco = (err, file, inputElem, reason) => {
    console.log(err)

  }
  handleOnErrorvc = (err, file, inputElem, reason) => {
    console.log(err)

  }

  handleOnRemoveFile = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.setState({ ebom: null })
  }

  handleRemoveFile = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (buttonRef.current) {
      buttonRef.current.removeFile(e)
    }
    this.setState({ ebom: false })
  }
  handleRemoveFileco = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (coRef.current) {
      coRef.current.removeFile(e)
    }
    this.setState({ cone: null })
  }
  handleRemoveFilevc = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (vcRef.current) {
      vcRef.current.removeFile(e)
    }
    this.setState({ voicecoil: null })
  }

  handleOnRemoveFileco = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.setState({ cone: null })
  }
  handleOnRemoveFilevc = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.setState({ voicecoil: null })
  }

  calculateebom = () => {
    this.setState({ proses: 'otw' })
    setTimeout(() => this.calculateebomnext(), 10)


  }
  calculateebomnext = () => {


    let { hasil, hasilcone, hasilvc } = this.state
    let hasiltotal = hasil.filter(val => {
      return val.level == "1"
    })
    console.log('satu')
    hasil = hasil.filter(val => {
      if (val.part) {
        return val.part.slice(0, 3) == "2VC" || (val.part.slice(0, 3) == "3CO" && parseInt(val.part.slice(3, 4)))
      }
    })
    console.log('dua')
    let speaker = [...new Set(hasil.map(item => item.speaker))];
    console.log(speaker)
    console.log('tiga')
    let speakernormal = []
    let speakerabnormal = []
    let speakernormallist = []
    let speakerabnormallist = []
    speaker.map(val => {
      let jumlahlv1 = hasiltotal.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.level == "1" && v.part_desc.includes("WF")
        }
      }).length

      console.log(jumlahlv1)
      let voicecoil = hasil.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.part.slice(0, 3) == "2VC"
        }
      })
      let levelvoicecoil = 10
      voicecoil.map(v => {
        if (parseInt(v.level) < levelvoicecoil) {
          levelvoicecoil = parseInt(v.level)
        }
      })
      let jumlahvc = voicecoil.filter(v => {
        return parseInt(v.level) == levelvoicecoil
      })

      let cone = hasil.filter(v => {
        if (v.part != null) {
          return v.speaker == val && v.part.slice(0, 3) == "3CO"
        }
      })

      let levelcone = 10
      cone.map(v => {
        if (parseInt(v.level) < levelcone) {
          levelcone = parseInt(v.level)
        }
      })
      let jumlahcone = cone.filter(v => {
        return parseInt(v.level) == levelcone
      })


      if (jumlahvc.length == 1 && jumlahcone.length == 1 && jumlahlv1 == 0) {
        speakernormallist.push(val)
        let cone = hasilcone.find(y => { return y.cone == jumlahcone[0].part })
        let vc = hasilvc.find(y => { return y.partnum == jumlahvc[0].part })
        let idminco = cone ? cone.idmin : 0
        let odaplikasi = vc ? vc.odaplikasi : 0
        let gap = (parseFloat(idminco) - parseFloat(odaplikasi))/2

        speakernormal.push({
          speaker: val,
          desc: jumlahvc[0].desc,
          vc: jumlahvc[0].part,
          co: jumlahcone[0].part,
          idco: cone ? cone.id : 0,
          tolminco: cone ? cone.tolmin : 0,
          tolmaxco: cone ? cone.tolmax : 0,
          idminco: cone ? cone.idmin : 0,
          idmax: cone ? cone.idmax : 0,
          vc: jumlahvc[0].part,
          vcid: vc ? vc.id : 0,
          vcidmax: vc ? vc.idmax : 0,
          vctebalformer: vc ? vc.tebalformer : 0,
          vctolmaxtebalformer: vc ? vc.tolmaxtebalformer : 0,
          vctebaltaping1: vc ? vc.tebaltaping1 : 0,
          vctebaltaping2: vc ? vc.tebaltaping2 : 0,
          vcdiawire: vc ? vc.diawire : 0,
          vcodaplikasi: vc ? vc.odaplikasi : 0,
          gap: parseFloat(gap).toFixed(2)

        })
      } else if (jumlahvc.length > 0 && jumlahcone.length > 0) {
        speakerabnormallist.push(val)
      }

    })
    console.log(speakernormal)
    console.log(speakernormallist)
    this.setState({
      speakerabnormallist: speakerabnormallist,
      speakerabnormal: speakerabnormal,
      speakernormal: speakernormal,
      speakernormallist: speakernormallist,
      hasiltotal: hasiltotal,
      proses: 'done'
    })


  }
  calculatelanjut = () => {
    this.setState({ proseslanjut: 'otw' })
    setTimeout(() => this.calculatelanjutnext(), 10)
  }

  calculatelanjutnext = () => {
    let { speakerabnormallist, hasiltotal, speakernormal, speakerabnormal, speakernormallist, proseslanjut } = this.state
    speakerabnormallist.map(val => {
      console.log('jalananeh')
      let abnormallv1 = hasiltotal.filter(v => {
        return v.level == "1" && speakernormallist.includes(v.part) && v.speaker == val
      })
      let partabnormal = []
      abnormallv1.map(v => {
        partabnormal.push(speakernormal.find(w => {
          return w.speaker == v.part
        }))
      })
      if (partabnormal.length > 0) {
        speakerabnormal.push({
          speaker: val, desc: abnormallv1[0].desc, subspeaker: partabnormal
        })
      }
    })
    console.log(speakerabnormal)
    this.setState({ speakerabnormal: speakerabnormal, proseslanjut: 'done' })
  }


  render() {
    let { ebom, jumlahebom, hasilcone, cone, voicecoil, hasilvc, proses, speakernormal, proseslanjut, speakerabnormal } = this.state
    let csvDataNormal = []
    let csvDataSpecial = []
    return (
      <>
        <Hay title='Part To Part Analysis (Voice Coil and Cone Paper)'>
          <Container>
            <div className="m-lg-3">
              <CSVReader
                ref={buttonRef}
                onFileLoad={this.handleOnFileLoad}
                onError={this.handleOnError}
                onDrag={this.handleOnFileLoad}
                addRemoveButton
                onRemoveFile={this.handleOnRemoveFile}
              >
                <span>Click to upload E-BOM</span>
              </CSVReader>
              <div className="mt-3">
                {
                  ebom == null ? (
                    <Alert variant={'warning'}>
                      Anda belum masukin Ebom
                    </Alert>
                  ) : (ebom == true ? (
                    <Alert variant={'primary'}>
                      {jumlahebom ? jumlahebom : 0} Data Ebom Berhasil di Upload
                    </Alert>
                  ) : (
                      <Alert variant={'danger'}>
                        Ebom anda salah format, tolong perbaiki
                      </Alert>
                    ))
                }
              </div>
            </div>
            {ebom ? (
              <div className="m-lg-3">
                <CSVReader
                  ref={coRef}
                  onFileLoad={this.handleOnFileLoadco}
                  onError={this.handleOnError}
                  onDrag={this.handleOnFileLoadco}
                  addRemoveButton
                  onRemoveFile={this.handleOnRemoveFileco}
                >
                  <span>Click to upload Database Cone Paper</span>
                </CSVReader>
                <div className="mt-3">
                  {
                    cone == null ? (
                      <Alert variant={'warning'}>
                        Anda belum masukin Database Cone Paper
                      </Alert>
                    ) : (cone == true ? (
                      <Alert variant={'primary'}>
                        {hasilcone.length > 0 ? hasilcone.length : 0} Data Conepaper Berhasil di Upload
                      </Alert>
                    ) : (
                        <Alert variant={'danger'}>
                          Database Cone Paper anda salah format, tolong perbaiki
                        </Alert>
                      ))
                  }

                </div>
              </div>) : false}
            {cone ? (
              <div className="m-lg-3">
                <CSVReader
                  ref={vcRef}
                  onFileLoad={this.handleOnFileLoadvc}
                  onError={this.handleOnError}
                  onDrag={this.handleOnFileLoadvc}
                  addRemoveButton
                  onRemoveFile={this.handleOnRemoveFilevc}
                >
                  <span>Click to upload Database Voice Coil</span>
                </CSVReader>
                <div className="mt-3">
                  {
                    voicecoil == null ? (
                      <Alert variant={'warning'}>
                        Anda belum masukin Database Voice Coil
                      </Alert>
                    ) : (voicecoil == true ? (
                      <Alert variant={'primary'}>
                        {hasilvc.length > 0 ? hasilvc.length : 0} Data Voice Coil Berhasil di Upload
                      </Alert>
                    ) : (
                        <Alert variant={'danger'}>
                          Database Voice Coil anda salah format, tolong perbaiki
                        </Alert>
                      ))
                  }

                </div>
              </div>) : false}

            <div className="m-3">
              {proses == 'otw' ? (
                <Alert variant={'warning'}>Masih Proses, Harap Tunggu</Alert>
              ) : proses == 'done' ? (<Alert variant={'success'}>Data Sudah Siap</Alert>) : false}
            </div>
            {voicecoil == true ? (
              <Button variant="primary" className="m-3"
                onClick={this.calculateebom
                }
                size="lg" block>
                Quick Count
              </Button>) : false
            }
          </Container>
          {
            speakernormal.length > 0 ? (
              <Container>
                <Table responsive striped bordered hover size="sm" style={{ fontSize: '0.6rem' }}>
                  <thead>
                    <tr>
                      {
                        ['No',
                          'Speaker',
                          'Deskripsi',
                          '3CO',
                          'id 3CO',
                          'id min 3CO',
                          '2VC',
                          'id 2VC',
                          'id max 2VC',
                          'tbl FR',
                          'tbl tapping1',
                          'tebal tapping2',
                          'dia wire',
                          'od aplikasi VC',
                          'gap'].map(val => {
                            return <th>{val}</th>
                          })
                          
                      }
                      {
                        csvDataNormal.push(['No',
                        'Speaker',
                        'Deskripsi',
                        '3CO',
                        'id 3CO',
                        'id min 3CO',
                        '2VC',
                        'id 2VC',
                        'id max 2VC',
                        'tbl FR',
                        'tbl tapping1',
                        'tebal tapping2',
                        'dia wire',
                        'od aplikasi VC',
                        'gap'])
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {speakernormal.map((val, i) => {
                      csvDataNormal.push([i + 1,
                        val.speaker,
                        val.desc,
                        val.co,
                        val.idco,
                        val.idminco,
                        val.vc,
                        val.vcid,
                        val.vcidmax,
                        val.vctebalformer,
                        val.vctebaltaping1,
                        val.vctebaltaping2,
                        val.vcdiawire,
                        val.vcodaplikasi,
                        val.gap])

                      return (
                        <tr>
                          {
                            [i + 1,
                            val.speaker,
                            val.desc,
                            val.co,
                            val.idco,
                            val.idminco,
                            val.vc,
                            val.vcid,
                            val.vcidmax,
                            val.vctebalformer,
                            val.vctebaltaping1,
                            val.vctebaltaping2,
                            val.vcdiawire,
                            val.vcodaplikasi,
                            val.gap].map(v => {
                              return <td>{v}</td>
                            })
                          }
                        </tr>)
                    })}
                  </tbody>
                </Table>
                <CSVLink data={csvDataNormal}>
                  <Button variant="success">Export ke CSV</Button>
                </CSVLink>

                <div className="m-3">
                  {proseslanjut == 'otw' ? (
                    <Alert variant={'warning'}>Jalan Masih Panjang, Harap Bersabar Guys</Alert>
                  ) : proseslanjut == 'done' ? (<Alert variant={'success'}>Data Sudah Siap</Alert>) : false}
                </div>
                <Button variant="danger" className="m-3"
                  onClick={this.calculatelanjut
                  }
                  size="lg" block>
                  Detailed Count
              </Button>



              </Container>) : false}
          {speakerabnormal.length > 0 ? (
            <Container>
              <Table responsive striped bordered hover size="sm" style={{ fontSize: '0.6rem' }}>
                <thead>
                  <tr>
                    {
                      ['No',
                        'Speaker',
                        'Deskripsi',
                        'Sub Speaker',
                        'Deskripsi',
                        '3CO',
                        'id 3CO',
                        'id min 3CO',
                        '2VC',
                        'id 2VC',
                        'id max 2VC',
                        'tbl FR',
                        'tbl tapping1',
                        'tebal tapping2',
                        'dia wire',
                        'od aplikasi VC',
                        'gap'
                      ].map(val => {
                        return <th>{val}</th>
                      })
                    }
                    {
                      csvDataSpecial.push(['No',
                      'Speaker',
                      'Deskripsi',
                      'Sub Speaker',
                      'Deskripsi',
                      '3CO',
                      'id 3CO',
                      'id min 3CO',
                      '2VC',
                      'id 2VC',
                      'id max 2VC',
                      'tbl FR',
                      'tbl tapping1',
                      'tebal tapping2',
                      'dia wire',
                      'od aplikasi VC',
                      'gap'
                    ])
                    }
                  </tr>
                </thead>

                <tbody>
                  {speakerabnormal.map((y, i) => {
                    let no = i + 1
                    return (
                      y.subspeaker.map((val) => {
                        csvDataSpecial.push([no,
                          y.speaker,
                          y.desc,
                          val.speaker,
                          val.desc,
                          val.co,
                          val.idco,
                          val.idminco,
                          val.vc,
                          val.vcid,
                          val.vcidmax,
                          val.vctebalformer,
                          val.vctebaltaping1,
                          val.vctebaltaping2,
                          val.vcdiawire,
                          val.vcodaplikasi,
                          val.gap])
                        return (
                          <tr>{
                            [no,
                              y.speaker,
                              y.desc,
                              val.speaker,
                              val.desc,
                              val.co,
                              val.idco,
                              val.idminco,
                              val.vc,
                              val.vcid,
                              val.vcidmax,
                              val.vctebalformer,
                              val.vctebaltaping1,
                              val.vctebaltaping2,
                              val.vcdiawire,
                              val.vcodaplikasi,
                              val.gap].map(v => {
                                return <td>{v}</td>
                              })
                          }
                          </tr>
                        )
                      })

                    )

                  })}
                </tbody>
              </Table>
              <CSVLink data={csvDataSpecial}>
                  <Button variant="success">Export ke CSV</Button>
                </CSVLink>
            </Container>
          ) : false}
        </Hay>
      </>

    )
  }
}
