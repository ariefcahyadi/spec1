import { FormControl, Layout, Button, Container, NavDropdown, Jumbotron } from 'react-bootstrap'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import React from 'react'
import { render } from 'react-dom'
import { Link, navigate } from "gatsby"


const Hay = (props) => {
        return (
                <>
                        <Container>
                                <Navbar bg="primary" variant="dark" fixed={'top'}>
                                        <Navbar.Brand href="#home">Product Engineering SBE</Navbar.Brand>
                                        <Nav className="mr-auto">
                                                <Nav.Link href="#home">Home</Nav.Link>
                                                <NavDropdown title="Fitur" id="basic-nav-dropdown">
                                                        <NavDropdown.Item onClick={() => { navigate('/') }}>Analysis VC & CO</NavDropdown.Item>
                                                        <NavDropdown.Item onClick={() => { navigate('/vctp/') }}>Analysis VC & TP</NavDropdown.Item>
                                                </NavDropdown>


                                        </Nav>
                                        <Form inline>
                                                <Nav.Link style={{ color: 'white' }}>Made With Love - Arief Cahyadi</Nav.Link>
                                                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                                                <Button variant="outline-light">Search</Button>
                                        </Form>
                                </Navbar>
                        </Container>
                        <Jumbotron fluid>
                                <Container>
                                        <h1>{props.title?props.title:''}</h1>
                                        <p>
                                                {props.description?props.description:''}
                                        </p>
                                </Container>
                        </Jumbotron>
                        <Container>
                                {props.children}
                        </Container>
                </>
        )

}

export default Hay